;( function( window ) {

  'use strict';
    
  /**
   * Extend obj function
   *
   * This is an object extender function. It allows us to extend an object
   * by passing in additional variables and overwriting the defaults.
   */
  function extend( a, b ) {
    for( var key in b ) { 
      if( b.hasOwnProperty( key ) ) {
        a[key] = b[key];
      }
    }
    return a;
  }
    

  /**
   * PKGradientFader
   */
  function PKGradientFader( options ) {
    // function body...
     this.options = extend( {}, this.options );
     extend( this.options, options ); 
     this._init(); 
  }
    
   /**
   * PKGradientFader options Object
   *
   * @type {HTMLElement} wrapper - The wrapper to create the ray in.
   */
  PKGradientFader.prototype.options = {
      src : document.body,
      sw : 300,
      sh : 250,
      direction : 'vertical'
  }

  PKGradientFader.prototype._init = function() {
      // create element
      this.src = this.options.src;
      //this.el.className = 'rogers-logo ' + this.options.type;
      TweenLite.set(this.src,{alpha:0});
      
      this.canv = document.createElement('canvas');
      this.canv.width = this.options.sw;
      this.canv.height = this.options.sh;
      this.canv.style.position = 'absolute';
      this.ctx = this.canv.getContext('2d');
      
      this.src.parentElement.insertBefore( this.canv, this.src );
      
      this.progress = 0;
      this.update();
      
      /*<div class="gwd-div-mbtx" id="ray">
            <div class="gwd-div-1t58 gwd-div-104l" id="leftwing" style=""></div>
            <div class="gwd-div-1t58 gwd-div-13r5 gwd-div-10lq" id="body" style=""></div>
            <div class="gwd-div-1t58 gwd-div-13r5 gwd-div-1f3e" id="tail" style="">
              <div class="gwd-div-1t58 gwd-div-13r5 gwd-div-1v04" id="barb" style=""></div>
            </div>
            <div class="gwd-div-1t58 gwd-div-13r5 gwd-div-1snj" id="rightwing" style=""></div>
          </div>*/

      // create html
      //var strinner = '';
      
      //this.el.innerHTML = strinner;

      // run the events
      //this._events();
    };  
    
    /**
     * PKGradientFader show
     *
     * This function starts our ray moving.
     * assumes GSAP.
     */
    PKGradientFader.prototype.show = function( timing, delay) {
        TweenLite.to( this, timing, {delay:delay,progress:1,onUpdate:this.update,onUpdateScope:this} );
    }
                                      
    PKGradientFader.prototype.update = function() {
        
        this.ctx.clearRect(0, 0, this.options.sw, this.options.sh);
        // draw mask.
        this.ctx.globalCompositeOperation = "source-over";
        //this.ctx.drawImage(gwd.text1inner2, 0, 0);
        
        // gradient calculations.
        var xstop = this.options.direction=='horizontal'?this.options.sw:0;
        var ystop = this.options.direction=='vertical'?this.options.sh:0;
        var grd = this.ctx.createLinearGradient(0,0,xstop,ystop);
        
        var stop1 = Math.max( 0, this.progress - .5 ) / .5;
        var val1 = Math.min( 1, this.progress / .5 );
        var stop2 = Math.min( 1, this.progress / .5 );
        var val2 = Math.max( 0, this.progress - .5 ) / .5;
        
        
        grd.addColorStop(stop1,'rgba(0,0,0,'+val1+')');
        grd.addColorStop(stop2,'rgba(0,0,0,'+val2+')');
        //console.log(this.progress, xstop, ystop, stop1, val1, stop2, val2);
        this.ctx.fillStyle = grd;
        this.ctx.fillRect(0,0,this.options.sw, this.options.sh);
        // draw fill
        this.ctx.globalCompositeOperation = "source-atop";
        this.ctx.drawImage(this.src, 0, 0);
    }
    
  /**
   * Add to global namespace
   */
  window.PKGradientFader = PKGradientFader;

})( window );